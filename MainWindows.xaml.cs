using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Griglia_bottone_viaggiatore
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Button B1 = new Button();
        public MainWindow()
        {
            Grid miogrid = new Grid();
            miogrid.Background = Brushes.Green;
            for(int i = 0; i < 10; i++)
            {
                miogrid.ColumnDefinitions.Add(new ColumnDefinition());
            }  
            for(int i = 0; i < 10; i++)
            {
                miogrid.RowDefinitions.Add(new RowDefinition());
            }
            miogrid.ShowGridLines = true;
            miogrid.Children.Add(B1);
            Grid.SetColumn(B1, 0);
            Grid.SetRow(B1, 0);
            
            InitializeComponent();
            mg.AddChild(miogrid);
            mg.KeyDown += Direzione;

        }
       void Direzione(object sender, KeyEventArgs e)
        {
            //  B1.FontSize = 7;
            B1.Content = "hello";
            B1.FontSize = 7;
            // risolevere i bug degli zeri 
            int br;
            int bc;
            if (e.Key.Equals(Key.W))
            {
                br = Grid.GetRow(B1);
                if (br <= 0)
                {
                    br = 10;
                }
                Grid.SetRow(B1, br - 1);
                br = 0;

            }
            if (e.Key.Equals(Key.S))
            {
                br = Grid.GetRow(B1);
                if (br >= 10)
                {
                    br = 0;
                }
                Grid.SetRow(B1, br + 1);
                br = 0;
            }
            if (e.Key.Equals(Key.D))
            {
                bc = Grid.GetColumn(B1);
                if (bc >= 10)
                {
                    bc = 0;
                }
                Grid.SetColumn(B1, bc + 1);
                bc = 0;
            }
            if (e.Key.Equals(Key.A))
            {
                bc = Grid.GetColumn(B1);
                if (bc <= 0)
                {
                    bc = 10;
                }
                Grid.SetColumn(B1, bc - 1);
                bc = 0;
            }
        }
    }
}